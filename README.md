### It's Pranav here 👋👋
#### A Proud Linux user

- 🔭 I’m currently working on AliceDOS, a command-line based operating system, with [Aman Bhatnagar](https://github.com/AmanBhatnagar12)
- 🌱 I’m currently learning C++ and some Web development 
- 📫 Connect with me at [Twitter](https://twitter.com/TheProudLinuxer)
- 😄 Pronouns: he/him
